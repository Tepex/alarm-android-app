Параметры устройства передаются один раз - при регистрации
В preferences на телефоне хранится user_id и характеризует авторизацию пользователя (его сессию).
Сессия бесконечна.

Авторизация:
POST /alarm/login
{
	login=login
	pwd=pwd
}

Ответ успешной авторизации:
{
	status: true
	userId: 12312
	ping: 100 // период пинга в с
}

Или неудачная авторизация:
{
	status: false
	msg: "error message"
}

Регистрация:
POST /alarm/register
model=
serial=
platform=
login=
pwd=
email=
name=
latitude=
longitude=

Удачная регистрация:
{
	status: true
	userId: 123123
	ping: 100 // период пинга в с
}

Пользователь уже есть:
{
	status: false
	msg: "error message"
}

Пинг:
GET /alarm/ping?device_id=222&lt=55.23423423&lg=38.12312312
{
	status: true
	ping: 100 // период пинга в с
}
package net.tepex.alarm.app;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.content.SharedPreferences;
import android.widget.Toast;
import android.telephony.TelephonyManager;
import android.location.Location;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import org.apache.http.Header;

import net.tepex.alarm.app.entities.Devices;
import net.tepex.alarm.app.Utils;
import static net.tepex.alarm.app.Utils.TAG;

public class RegistrationActivity extends Activity
{
	@Override
	public void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		setContentView(R.layout.reg_layout);
		regButton = (Button)findViewById(R.id.reg_button1);
		progressDialog = new ProgressDialog(this);
		progressDialog.setMessage("Подождите...");
		progressDialog.setCancelable(false);
	}
	
	/**
	 * Listener кнопки
	 */
	public void registerUser(View view)
	{
		RequestParams params = new RequestParams();
		
		String model = Devices.getDeviceName();
		TelephonyManager tManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
		String uid = tManager.getDeviceId();
		
		EditText loginEdit = (EditText)findViewById(R.id.reg_login_edit);
		String login = loginEdit.getText().toString();
		EditText nameEdit = (EditText)findViewById(R.id.reg_name_edit);
		String name = nameEdit.getText().toString();
		EditText emailEdit = (EditText)findViewById(R.id.reg_email_edit);
		String email = emailEdit.getText().toString();
		EditText pwdEdit = (EditText)findViewById(R.id.reg_pwd_edit);
		String pwd = pwdEdit.getText().toString();
		
		if(Utils.isNotNull(login) && Utils.isNotNull(name) && Utils.isNotNull(email) && Utils.isNotNull(pwd))
		{
			Location location = Utils.getLocation(getApplicationContext());
			double lat = 0;
			double lng = 0;
			if(location != null)
			{
				lat = location.getLatitude();
				lng = location.getLongitude();
			}
			params.put("model", model);
			params.put("serial", uid);
			params.put("platform", "Android");
			params.put("login", new String(login));
			params.put("name", new String(name));
			params.put("email", new String(email));
			params.put("pwd", new String(pwd));
			params.put("latitude", lat);
			params.put("longitude", lng);
			invokeWS(params);
		}
		else
		{
			Toast.makeText(getApplicationContext(), "Не заполнены поля формы!", Toast.LENGTH_LONG).show();
		}
	}
	
	private void invokeWS(RequestParams params)
	{
		regButton.setEnabled(false);
		progressDialog.show();
		
		AsyncHttpClient client = new AsyncHttpClient(Utils.PORT);
		//client.setResponseTimeout(40000);
		//client.setTimeout(40000);
		client.addHeader(AsyncHttpClient.HEADER_CONTENT_TYPE, "application/x-www-form-urlencoded; charset=utf-8");
		String url = Utils.getServerURL()+"register";
		Log.d(Utils.TAG, "url: "+url);
		client.post(url, params, new AsyncHttpResponseHandler()
		{
			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] responseBody)
			{
				progressDialog.hide();
				regButton.setEnabled(true);
				try
				{
					JSONObject obj = new JSONObject(new String(responseBody));
					if(statusCode == 201)
					{
						long userId = obj.getLong(Utils.USER_ID);
						int pingDelay = obj.getInt(Utils.PING);
						SharedPreferences.Editor preferences = getApplicationContext().getSharedPreferences(BaseActivity.APP_SHARED_PREFS, Context.MODE_PRIVATE).edit();
						preferences.putLong(Utils.USER_ID, userId);
						preferences.putInt(Utils.PING, pingDelay);
						preferences.apply();
						letsGo(pingDelay);
					}
					else Toast.makeText(getApplicationContext(), obj.getString("msg"), Toast.LENGTH_LONG).show();
				}
				catch(JSONException e)
				{
					Toast.makeText(getApplicationContext(), "Ошибка сервера (JSON)!", Toast.LENGTH_LONG).show();
					e.printStackTrace();
				}
			}
			
			@Override
			public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error)
			{
				Log.e(Utils.TAG, "error code: "+statusCode);
				progressDialog.hide();
				regButton.setEnabled(true);
				if(statusCode == 404)
					Toast.makeText(getApplicationContext(), "Ресурс не найден!", Toast.LENGTH_LONG).show();
				else if(statusCode == 500)
					Toast.makeText(getApplicationContext(), "Ошибка сервера!", Toast.LENGTH_LONG).show();
				else
					Toast.makeText(getApplicationContext(), "Неизвестная ошибка (Возможно нет подключения к Сети или не работает сервер)!", Toast.LENGTH_LONG).show();
			}
        });
	}
	
	private void letsGo(int pingDelay)
	{
		progressDialog.dismiss();
		Intent intent = new Intent(getApplicationContext(), MainActivity.class);
		intent.putExtra(Utils.PING_DELAY, pingDelay);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
		finish();
	}
	
	private Button regButton;
	private ProgressDialog progressDialog;
}
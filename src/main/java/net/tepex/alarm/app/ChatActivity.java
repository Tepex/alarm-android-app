package net.tepex.alarm.app;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;

import android.util.Log;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.graphics.Color;

import android.widget.TextView;
import android.widget.Toast;
import android.widget.LinearLayout;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;

import net.tepex.alarm.app.entities.Message;
import net.tepex.alarm.app.entities.User;
import static net.tepex.alarm.app.Utils.TAG;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import org.apache.http.Header;

public class ChatActivity extends BaseActivity
{
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.chat_layout);
		friend = (User)getIntent().getExtras().getParcelable(INTENT_FRIEND);
		TextView friendNameView = (TextView)findViewById(R.id.friend_name_id);
		friendNameView.setText("Чат с товарищем "+friend.getName());
		inputMessageView = (TextView)findViewById(R.id.input_text);
		getChat();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		boolean ret = super.onCreateOptionsMenu(menu);
		menu.removeItem(R.id.action_messages);
		return ret;
	}
	
	public void sendMessage(View view)
	{
		String text = inputMessageView.getText().toString().trim();
		if(text.equals("")) return;
		final Message message = new Message(userId, friend.getId(), text, new Date());
		
		AsyncHttpClient client = new AsyncHttpClient(Utils.PORT);
		client.addHeader(AsyncHttpClient.HEADER_CONTENT_TYPE, "application/x-www-form-urlencoded; charset=utf-8");
		String url = Utils.getServerURL()+"messages/chat";
		RequestParams params = message.getRequestParams();
		client.post(url, params, new AsyncHttpResponseHandler()
		{
			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] responseBody)
			{
				try
				{
					String response = new String(responseBody);
					JSONObject json = new JSONObject(response);
					if(statusCode == 201)
					{
						long messageId = json.getLong(Message.ID);
						message.setId(messageId);
						chat.add(message);
						showMessage(message);
					}
					else
					{
						Toast.makeText(getApplicationContext(), json.getString("msg"), Toast.LENGTH_LONG).show();
					}
				}
				catch(JSONException e)
				{
					Toast.makeText(getApplicationContext(), "Ошибка сервера (JSON)!", Toast.LENGTH_LONG).show();
				}
			}
			
			@Override
			public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error)
			{
				if(statusCode == 404)
					Toast.makeText(getApplicationContext(), "Ресурс не найден!", Toast.LENGTH_LONG).show();
				else if(statusCode == 500)
					Toast.makeText(getApplicationContext(), "Ошибка сервера!", Toast.LENGTH_LONG).show();
				else
					Toast.makeText(getApplicationContext(), "Неизвестная ошибка (Возможно нет подключения к Сети или не работает сервер)!", Toast.LENGTH_LONG).show();
			}
        });
	}
	
	private void getChat()
	{
		AsyncHttpClient client = new AsyncHttpClient(Utils.PORT);
		String url = Utils.getServerURL()+"messages/chat";
		RequestParams params = new RequestParams();
		params.put(Message.PARAM_USER_ID, ""+userId);
		params.put(Message.PARAM_FRIEND_ID, ""+friend.getId());
		client.get(url, params, new AsyncHttpResponseHandler()
		{
			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] responseBody)
			{
				try
				{
					String response = new String(responseBody);
					if(statusCode == 200)
					{
						JSONArray jsonMessages = new JSONArray(response);
						for(int i = 0; i < jsonMessages.length(); i++)
						{
							Message msg = new Message(jsonMessages.getJSONObject(i));
							chat.add(msg);
							showMessage(msg);
						}
					}
					else
					{
						JSONObject obj = new JSONObject(response);
						Toast.makeText(getApplicationContext(), obj.getString("msg"), Toast.LENGTH_LONG).show();
					}
				}
				catch(JSONException e)
				{
					Toast.makeText(getApplicationContext(), "Ошибка сервера (JSON)!", Toast.LENGTH_LONG).show();
				}
			}
			
			@Override
			public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error)
			{
				if(statusCode == 404)
					Toast.makeText(getApplicationContext(), "Ресурс не найден!", Toast.LENGTH_LONG).show();
				else if(statusCode == 500)
					Toast.makeText(getApplicationContext(), "Ошибка сервера!", Toast.LENGTH_LONG).show();
				else
					Toast.makeText(getApplicationContext(), "Неизвестная ошибка (Возможно нет подключения к Сети или не работает сервер)!", Toast.LENGTH_LONG).show();
			}
        });
	}
	
	private void showMessage(Message msg)
	{
		LinearLayout messagesContainer = (LinearLayout)findViewById(R.id.messages_container);
		TextView dateGap = (TextView)getLayoutInflater().inflate(R.layout.messages_gap_tmpl, null);
		dateGap.setText(msg.getDateString());
		messagesContainer.addView(dateGap);
		
		int layoutId;
		if(msg.isInbox()) layoutId = R.layout.messages_left_tmpl;
		else layoutId = R.layout.messages_right_tmpl;
		LinearLayout messageBlockLayout = (LinearLayout)getLayoutInflater().inflate(layoutId, null);
		
		TextView messageView = new TextView(getApplicationContext());
		messageView.setTextColor(Color.BLACK);
		messageView.setText(msg.getText());
		ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		messageView.setLayoutParams(params);
		
		LinearLayout inner = new LinearLayout(getApplicationContext());
		inner.setLayoutParams(params);
		inner.setPaddingRelative(5, 5, 5, 5);
		if(msg.isInbox()) inner.setBackgroundColor(Color.YELLOW);
		else inner.setBackgroundColor(Color.parseColor("#00ffff"));
		inner.addView(messageView);
		
		messageBlockLayout.addView(inner);
		
		messagesContainer.addView(messageBlockLayout);
	}
	
	private User friend;
	private List<Message> chat = new ArrayList<Message>();
	private TextView inputMessageView;

	public static final String INTENT_FRIEND = "friend";
}

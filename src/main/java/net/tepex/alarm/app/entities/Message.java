package net.tepex.alarm.app.entities;

import java.util.Date;

import com.loopj.android.http.RequestParams;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.text.ParseException;

import org.json.JSONObject;
import org.json.JSONException;

public class Message
{
	public Message(JSONObject json) throws JSONException
	{
		id = json.getLong(ID);
		text = json.getString(TEXT);
		isInbox = json.getBoolean(IS_INBOX);
		dateString = json.getString(DATE);
		try
		{
			date = DF.parse(dateString);
		}
		catch(ParseException e) {}
	}
	
	public Message(long id, String text, boolean isInbox, Date date)
	{
		this.id = id;
		this.text = text;
		this.isInbox = isInbox;
		this.date = date;
		dateString = DF.format(date);
	}
	
	public Message(long from, long to, String text, Date date)
	{
		this.from = from;
		this.to = to;
		this.text = text;
		this.date = date;
		dateString = DF.format(date);
		isInbox = false;
	}
	
	public long getId()
	{
		return id;
	}
	
	public String getText()
	{
		return text;
	}
	
	public boolean isInbox()
	{
		return isInbox;
	}
	
	public Date getDate()
	{
		return date;
	}
	
	public String getDateString()
	{
		return dateString;
	}
	
	public void setId(long id)
	{
		this.id = id;
	}
	
	public RequestParams getRequestParams()
	{
		RequestParams params = new RequestParams();
		params.put(PARAM_FROM, ""+from);
		params.put(PARAM_TO, ""+to);
		params.put(PARAM_TEXT, text);
		params.put(PARAM_DATE, dateString);
		return params;
	}
	
	@Override
	public int hashCode()
	{
		return (int)id;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(obj == null) return false;
		if(!(obj instanceof Message)) return false;
		Message other = (Message)obj;
		return id == other.id;
	}
	
	private long id;
	private long from;
	private long to;
	private String text;
	private boolean isInbox;
	private Date date;
	private String dateString;
	
	public static final DateFormat DF = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
	
	public static final String ID = "id";
	public static final String TEXT = "text";
	public static final String IS_INBOX = "isInbox";
	public static final String DATE = "date";
	
	public static final String PARAM_USER_ID = "userId";
	public static final String PARAM_FRIEND_ID = "friendId";
	public static final String PARAM_FROM = "from";
	public static final String PARAM_TO = "to";
	public static final String PARAM_TEXT = "text";
	public static final String PARAM_DATE = "date";
}
package net.tepex.alarm.app;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.content.Context;
import android.os.Bundle;

public class Utils
{
	public static boolean isNotNull(String txt)
	{
		return txt != null && txt.trim().length() > 0 ? true: false;
	}
	
	public static String getServerURL()
	{
		return "http://"+HOST+":"+PORT+"/"+APP+"/";
	}
	
	public static Location getLocation(Context context)
	{
		if(myLocation == null) myLocation = new MyLocation(context);
		return myLocation.getLocation();
	}
	
	//public static final String HOST = "10.0.2.2";
	
	public static final String HOST = "tepex.net";
	public static final int PORT = 8080;
	public static final String APP = "alarm";
	
	public static final String TAG = "Alarm";
	
	public static final String USER_ID = "userId";
	public static final String PING_DELAY = "pingDelay";
	public static final String PING = "ping";
	
	private static MyLocation myLocation;
	
	private static class MyLocation implements LocationListener
	{
		public MyLocation(Context context)
		{
			locationManager = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
			locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 10, this);  
		}
		
		public Location getLocation()
		{
			return location;
		}
		
		@Override
		public void onLocationChanged(Location loc)
		{
			location = loc;
		}
		
		@Override
		public void onProviderDisabled(String provider) {}
		
		@Override
		public void onProviderEnabled(String provider) {}
		
		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {}
		
		private LocationManager locationManager;
		private Location location;
	}
}
package net.tepex.alarm.app;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;

import java.net.HttpURLConnection;
import java.net.URL;
import java.net.MalformedURLException;

import java.io.DataOutputStream;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

import android.content.SharedPreferences;
import android.util.Log;
import static net.tepex.alarm.app.Utils.TAG;

import com.google.android.gms.maps.model.LatLng;

public class PingThread extends Thread
{
	public PingThread(SharedPreferences preferences, int pingDelay, UsersManager usersManager)
	{
		super("pingThread");
		// перевод из секунд в миллисекунды
		this.preferences = preferences;
		this.pingDelay = pingDelay*1000;
		this.usersManager = usersManager;
	}
	
	@Override
	public void run()
	{
		long userId = preferences.getLong(Utils.USER_ID, 0L);
		isRunning = true;
		while(isRunning)
		{
			String file = Utils.APP+"/ping?"+Utils.USER_ID+"="+userId+"&"+PARAM_LATITUDE+"="+MSK.latitude+"&"+PARAM_LONGITUDE+"="+MSK.longitude;
			StringBuilder sb = new StringBuilder();
			try
			{
				URL url = new URL("http", Utils.HOST, Utils.PORT, file);
				HttpURLConnection connection = (HttpURLConnection)url.openConnection();
				connection.setRequestMethod("PUT");
				connection.setRequestProperty("charset", "utf-8");
				connection.setUseCaches(false);
				Reader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
				for(int c = reader.read(); c != -1; c = reader.read())
				{
					sb.append((char)c);
				}
				reader.close();
				connection.disconnect();
			}
			catch(IOException e)
			{
				Log.e(TAG, "Request error", e);
			}
			//Log.d(TAG, "ping answer: "+sb);
			try
			{
				JSONObject json = new JSONObject(sb.toString());
				pingDelay = json.getInt("ping");
				//Log.d(TAG, "new ping delay "+pingDelay);
				JSONArray jsonUsers = json.getJSONArray("online");
				//Log.d(TAG, "jsonUsers "+jsonUsers);
				usersManager.updateUsers(jsonUsers);
			}
			catch(JSONException e)
			{
				Log.e(TAG, "ping answer error "+e.getMessage());
			}
			
			try
			{
				sleep(pingDelay);
			}
			catch(InterruptedException e)
			{
				return;
			}
		}
	}
	
	public void exit()
	{
		isRunning = false;
		interrupt();
		try
		{
			join();
		}
		catch(InterruptedException e)
		{
		}
	}
	
	private SharedPreferences preferences;
	private int pingDelay;
	private boolean isRunning;
	private UsersManager usersManager;
	
	public static final String PARAM_LATITUDE = "latitude";
	public static final String PARAM_LONGITUDE = "longitude";
	
	private static final LatLng MSK = new LatLng(55.725595, 37.443137);
}
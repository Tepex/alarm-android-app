package net.tepex.alarm.app.entities;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Date;

import org.json.JSONObject;
import org.json.JSONException;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import android.os.Parcelable;
import android.os.Parcel;

/**
 * Реализация пользователя.
 *
 * @author Tepex <tepex@mail.ru>
 */
public class User implements Parcelable
{
	public User(JSONObject json) throws JSONException
	{
		id = json.getLong(PARAM_ID);
		login = json.getString(PARAM_LOGIN);
		email = json.getString(PARAM_EMAIL);
		name = json.getString(PARAM_NAME);
		isBot = json.getBoolean(PARAM_IS_BOT);
		isAdmin = json.getBoolean(PARAM_IS_ADMIN);
		position = new LatLng(json.getDouble(PARAM_LATITUDE), json.getDouble(PARAM_LONGITUDE));
		try
		{
			lastAccess = DF.parse(json.getString(PARAM_LAST_ACCESS));
			created = DF.parse(json.getString(PARAM_CREATED));
		}
		catch(ParseException e)
		{
			
		}
	}
	
	public long getId()
	{
		return id;
	}
	
	/**
	 * Возвращает логин пользователя.
	 * @return String Логин пользователя.
	 */
	public String getLogin()
	{
		return login;
	}
	
	/**
	 * Возвращает Email пользователя.
	 * @return String Email пользователя.
	 */
	public String getEmail()
	{
		return email;
	}
	
	/**
	 * Возвращает имя пользователя.
	 * @return String Имя пользователя.
	 */
	public String getName()
	{
		return name;
	}
	
	/**
	 * Возвращает признак бота.
	 * @return boolean Признак бота.
	 */
	public boolean isBot()
	{
		return isBot;
	}
	
	public boolean isAdmin()
	{
		return isAdmin;
	}
	
	/**
	 * Возвращает время последнего доступа.
	 * @return Date Время последнего доступа.
	 */
	public Date getLastAccess()
	{
		return lastAccess;
	}
	
	public LatLng getPosition()
	{
		return position;
	}
	
	/**
	 * Возвращает дату создания.
	 * @return Date Дата создания.
	 */
	public Date getCreatedDate()
	{
		return created;
	}
	
	public Marker getMarker()
	{
		return marker;
	}
	
	public void setMarker(Marker marker)
	{
		this.marker = marker;
	}
	
	public void updatePosition(JSONObject json) throws JSONException
	{
		position = new LatLng(json.getDouble(PARAM_LATITUDE), json.getDouble(PARAM_LONGITUDE));
		marker.setPosition(position);
	}
	
	@Override
	public int hashCode()
	{
		return (int)id;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(obj == null) return false;
		if(!(obj instanceof User)) return false;
		User other = (User)obj;
		return other.id == id;
	}
	
	@Override
	public String toString()
	{
		return name+" ["+login+" ("+id+")]";
	}
	
	@Override
	public int describeContents()
	{
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel out, int flags)
	{
		out.writeLong(id);
		out.writeString(login);
		out.writeString(email);
		out.writeString(name);
		out.writeByte((byte)(isBot ? 1 : 0));
		out.writeByte((byte)(isAdmin ? 1 : 0));
		out.writeParcelable(position, 0);
	}
	
	private User(Parcel in)
	{
		id = in.readLong();
		login = in.readString();
		email = in.readString();
		name = in.readString();
		isBot = in.readByte() != 0;
		isAdmin = in.readByte() != 0;
		position = (LatLng)in.readParcelable(LatLng.class.getClassLoader());
	}

	
	private long id;
	private String login;
	private String email;
	private String name;
	private boolean isBot;
	private boolean isAdmin;
	private LatLng position;
	private Date lastAccess;
	private Date created;
	private Marker marker;
	
	public static final String PARAM_ID = "id";
	public static final String PARAM_LOGIN = "login";
	public static final String PARAM_EMAIL = "email";
	public static final String PARAM_NAME = "name";
	public static final String PARAM_IS_BOT = "isBot";
	public static final String PARAM_IS_ADMIN = "isAdmin";
	public static final String PARAM_LATITUDE = "latitude";
	public static final String PARAM_LONGITUDE = "longitude";
	public static final String PARAM_LAST_ACCESS = "lastAccess";
	public static final String PARAM_CREATED = "created";
	
	public static final DateFormat DF = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
	
	public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>()
	{
		public User createFromParcel(Parcel in)
		{
			return new User(in);
		}
		
		public User[] newArray(int size)
		{
			return new User[size];
		}
	};
}
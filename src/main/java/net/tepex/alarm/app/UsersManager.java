package net.tepex.alarm.app;

import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;

import android.app.Activity;
import android.util.Log;

import static net.tepex.alarm.app.Utils.TAG;
import net.tepex.alarm.app.entities.User;

public class UsersManager
{
	public UsersManager(Activity activity, GoogleMap gMap)
	{
		this.activity = activity;
		this.gMap = gMap;
	}
	
	public void updateUsers(final JSONArray jsonUsers)
	{
		activity.runOnUiThread(new Runnable()
			{
				public void run()
				{
					Set<Long> changed = new HashSet<Long>();
					try
					{
						for(int i = 0; i < jsonUsers.length(); i++)
						{
							JSONObject jsonItem = jsonUsers.getJSONObject(i);
							long id = jsonItem.getLong("id");
							User user = users.get(id);
							if(user == null)
							{
								user = new User(jsonItem);
								users.put(id, user);
								Marker marker = gMap.addMarker(new MarkerOptions()
									.position(user.getPosition())
									.title(user.getName())
									.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
								user.setMarker(marker);
							}
							else user.updatePosition(jsonItem);
							changed.add(id);
						}
					}
					catch(JSONException e)
					{
						Log.e(TAG, "ping answer error "+e.getMessage());
					}
					// удаляем тех, кто не прошел обновление
					for(Iterator<Map.Entry<Long, User>> it = users.entrySet().iterator(); it.hasNext();)
					{
						Map.Entry<Long, User> entry = it.next();
						if(!changed.contains(entry.getKey()))
						{
							User user = entry.getValue();
							user.getMarker().remove();
							it.remove();
						}
					}
				}
			}
		);
	}
	
	public User findUser(Marker marker)
	{
		for(User user: users.values())
		{
			if(user.getMarker().getId().equals(marker.getId())) return user;
		}
		return null;
	}
	
	private Map<Long, User> users = new HashMap<Long, User>();
	private Activity activity;
	private GoogleMap gMap;
}

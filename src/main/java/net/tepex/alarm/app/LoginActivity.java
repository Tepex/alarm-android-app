package net.tepex.alarm.app;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import org.apache.http.Header;

import static net.tepex.alarm.app.Utils.TAG;

public class LoginActivity extends Activity
{
	@Override
	public void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		isUserLogon();
		setContentView(R.layout.login_layout);
		progressDialog = new ProgressDialog(this);
		progressDialog.setMessage("Подождите...");
		progressDialog.setCancelable(false);
	}
	
	@Override
	public void onStart()
	{
		super.onStart();
	}
	
	@Override
	public void onRestart()
	{
		isUserLogon();
		super.onRestart();
	}
	
	@Override
	public void onResume()
	{
		isUserLogon();
		super.onResume();
	}
	
	/**
	 * Listener кнопки
	 */
	public void loginUser(View view)
	{
		TextView loginView = (TextView)findViewById(R.id.login_view);
		TextView pwdView = (TextView)findViewById(R.id.pwd_view);
		String login = loginView.getText().toString();
		String pwd = pwdView.getText().toString();
		Log.d(TAG, "user: "+login+" pwd: "+pwd);
		RequestParams params = new RequestParams();
		if(Utils.isNotNull(login) && Utils.isNotNull(pwd))
		{
			params.put("login", login);
			params.put("pwd", pwd);
			invokeWS(params);
		}
		else
		{
			Toast.makeText(getApplicationContext(), "Не заполнены поля формы!", Toast.LENGTH_LONG).show();
		}
	}
	
	public void goRegistration(View view)
	{
		progressDialog.dismiss();
		Intent intent = new Intent(getApplicationContext(), RegistrationActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
		finish();
	}
	
	private void invokeWS(RequestParams params)
	{
		progressDialog.show();
		AsyncHttpClient client = new AsyncHttpClient(Utils.PORT);
		String url = Utils.getServerURL()+"login";
		client.put(url, params, new AsyncHttpResponseHandler()
		{
			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] responseBody)
			{
				progressDialog.hide();
				try
				{
					JSONObject obj = new JSONObject(new String(responseBody));
					if(statusCode == 200)
					{
						long userId = obj.getLong(Utils.USER_ID);
						Log.d(Utils.TAG, "userId="+userId);
						int pingDelay = obj.getInt(Utils.PING);
						SharedPreferences.Editor preferences = getApplicationContext().getSharedPreferences(BaseActivity.APP_SHARED_PREFS, Context.MODE_PRIVATE).edit();
						preferences.putLong(Utils.USER_ID, userId);
						preferences.putInt(Utils.PING, pingDelay);
						preferences.apply();
						letsGo(pingDelay);
					}
					else Toast.makeText(getApplicationContext(), obj.getString("msg"), Toast.LENGTH_LONG).show();
				}
				catch(JSONException e)
				{
					Toast.makeText(getApplicationContext(), "Ошибка сервера (JSON)!", Toast.LENGTH_LONG).show();
				}
			}
			
			@Override
			public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error)
			{
				progressDialog.hide();
				if(statusCode == 404)
					Toast.makeText(getApplicationContext(), "Ресурс не найден!", Toast.LENGTH_LONG).show();
				else if(statusCode == 500)
					Toast.makeText(getApplicationContext(), "Ошибка сервера!", Toast.LENGTH_LONG).show();
				else
					Toast.makeText(getApplicationContext(), "Неизвестная ошибка (Возможно нет подключения к Сети или не работает сервер)!", Toast.LENGTH_LONG).show();
			}
        });
	}
	
	private boolean isUserLogon()
	{
		SharedPreferences preferences = getApplicationContext().getSharedPreferences(BaseActivity.APP_SHARED_PREFS, Context.MODE_PRIVATE);
		long id = preferences.getLong(Utils.USER_ID, 0);
		if(id != 0) letsGo(0);
		return id != 0;
	}
	
	private void letsGo(int pingDelay)
	{
		progressDialog.dismiss();
		Intent intent = new Intent(getApplicationContext(), MainActivity.class);
		intent.putExtra(PING_DELAY, pingDelay);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
		finish();
	}
	
	private ProgressDialog progressDialog;
	
	public static final String PING_DELAY = "pingDelay";
}

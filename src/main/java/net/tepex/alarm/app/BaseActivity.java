package net.tepex.alarm.app;

import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuInflater;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import static net.tepex.alarm.app.Utils.TAG;

public class BaseActivity extends ActionBarActivity
{
	@Override
	public void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		SharedPreferences preferences = getApplicationContext().getSharedPreferences(APP_SHARED_PREFS, Context.MODE_PRIVATE);
		userId = preferences.getLong(Utils.USER_ID, 0);

		// http://stackoverflow.com/questions/17915393/android-login-activity-and-home-activity-redirection
		isUserLogon();
	}
	
	@Override
	public void onRestart()
	{
		isUserLogon();
		super.onRestart();
	}
	
	@Override
	public void onResume()
	{
		isUserLogon();
		super.onResume();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main_activity_actions, menu);
		messagesMenuItem = menu.findItem(R.id.action_messages);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId())
		{
			case R.id.action_exit:
				logout();
				return true;
			case R.id.action_users:
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
	
	private boolean isUserLogon()
	{
		if(userId == 0)
		{
			Intent intent = new Intent(this, LoginActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			finish();
		}
		return userId != 0;
	}
	
	private void logout()
	{
		SharedPreferences.Editor preferences = getApplicationContext().getSharedPreferences(BaseActivity.APP_SHARED_PREFS, Context.MODE_PRIVATE).edit();
		preferences.clear();
		preferences.apply();
		Intent intent = new Intent(this, LoginActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
		finish();
	}
	
	protected long userId;
	protected MenuItem messagesMenuItem;
	
	public static final String APP_SHARED_PREFS = "alarm_prefs";
}

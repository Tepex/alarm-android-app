package net.tepex.alarm.app;

import android.os.Bundle;
import android.content.Context;
import android.content.Intent;
import android.widget.TextView;
import android.widget.Toast;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuInflater;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.location.LocationListener;
import android.location.Location;

import static net.tepex.alarm.app.Utils.TAG;
import net.tepex.alarm.app.entities.User;

import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.CameraUpdateFactory;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import org.apache.http.Header;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;

import java.util.Map;
import java.util.HashMap;

public class MainActivity extends BaseActivity implements GoogleMap.OnInfoWindowClickListener, GoogleMap.OnMarkerClickListener, GoogleMap.OnMapClickListener
{
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_layout);
		
		pingDelay = getIntent().getIntExtra(LoginActivity.PING_DELAY, 5);
		Log.d(TAG, "ping delay "+pingDelay);
		if(pingDelay == 0) pingDelay = 5;

		LocationManager locationManager = (LocationManager)getSystemService(LOCATION_SERVICE);
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 60000, 50, locationListener);
		
		gMap = ((SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
		if(gMap != null)
		{
			gMap.setMyLocationEnabled(true);
			gMap.setOnInfoWindowClickListener(this);
			gMap.setOnMarkerClickListener(this);
			gMap.setOnMapClickListener(this);
			try
			{
				Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
				myPosition = new LatLng(location.getLatitude(), location.getLongitude());
				myMarker = gMap.addMarker(new MarkerOptions().position(myPosition).title("Я"));
				gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myPosition, 9));
			}
			catch(Exception e)
			{
				Log.e(TAG, "Exception: "+e);
			}
		}
		usersManager = new UsersManager(this, gMap);
	}
	
	@Override
	public void onStart()
	{
		super.onStart();
		SharedPreferences preferences = getApplicationContext().getSharedPreferences(BaseActivity.APP_SHARED_PREFS, Context.MODE_PRIVATE);
		pingThread = new PingThread(preferences, pingDelay, usersManager);
		pingThread.start();
	}
	
	@Override
	public void onStop()
	{
		if(pingThread != null) pingThread.exit();
		super.onStop();
	}
	
	@Override
	public void onInfoWindowClick(Marker marker)
	{
	}
	
	@Override
	public boolean onMarkerClick(Marker marker)
	{
		selectedUser = usersManager.findUser(marker);
		messagesMenuItem.setEnabled(true);
		return false;
	}
	
	@Override
	public void onMapClick(LatLng pos)
	{
		messagesMenuItem.setEnabled(false);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		if(item.getItemId() == R.id.action_messages && selectedUser != null)
		{
			Intent intent = new Intent(getApplicationContext(), ChatActivity.class);
			Bundle bundle = new Bundle();
			bundle.putParcelable(ChatActivity.INTENT_FRIEND, selectedUser);
			intent.putExtras(bundle);
			startActivity(intent);
		}
		return super.onOptionsItemSelected(item);
	}
	
	private GoogleMap gMap;
	private Marker myMarker;
	private LatLng myPosition;
	private int pingDelay;
	private PingThread pingThread;
	private User selectedUser;
	private UsersManager usersManager;
	
	public static final LatLng MOSCOW = new LatLng(55.752126, 37.617294);
	
	private final LocationListener locationListener = new LocationListener()
	{
		@Override
		public void onLocationChanged(final Location location)
		{
			myPosition = new LatLng(location.getLatitude(), location.getLongitude());
			gMap.moveCamera(CameraUpdateFactory.newLatLng(myPosition));
		}
		
		@Override
		public void onProviderDisabled(String provider) {}
		
		@Override
		public void onProviderEnabled(String provider) {}
		
		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {}
	};
}
